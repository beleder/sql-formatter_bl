/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package org.bitbucket.leito.sqlformatter;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.operators.relational.ExpressionList;
import net.sf.jsqlparser.expression.operators.relational.ItemsList;
import net.sf.jsqlparser.expression.operators.relational.MultiExpressionList;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.Statements;
import net.sf.jsqlparser.statement.insert.Insert;

/**
 * A class to formats and groups SQL content.
 */
public class SqlFormatter {

    /**
     * The list of insert statements to format. The key is the name of the
     * table.
     */
    private Map<String, InsertStatementFormatter> insertStatements = new LinkedHashMap<>();

    /**
     * Formats the sql script. This method only works with Insert statements. It
     * groups all insert for the same table into one statement. Then it formats
     * all the columns so that its easier to read.
     *
     * @param sql the SQL script to format. It may only contain insert
     * statements.
     * @return a formatted sql script.
     * @throws JSQLParserException if an error ocurrs.
     */
    public String format(String sql) throws JSQLParserException {
        Statements stmt = CCJSqlParserUtil.parseStatements(sql);
        List<Statement> statements = stmt.getStatements();
        for (Statement statement : statements) {
            if (statement instanceof Insert) {
                Insert insert = (Insert) statement;
                String tableName = insert.getTable().getName();
                addInsertStatement(tableName);
                addRowValues(tableName, insert.getColumns(), insert.getItemsList());
            } else {
                throw new UnsupportedOperationException("Statement not supported: " + statement.getClass());
            }
        }
        return formatInsertStatements();
    }

    private void addInsertStatement(String tableName) {
        insertStatements.putIfAbsent(tableName, new InsertStatementFormatter(tableName));
    }

    private void addRowValues(String tableName, List<Column> columns, ItemsList itemsList) {
        if (itemsList instanceof ExpressionList) {
            ExpressionList values = (ExpressionList) itemsList;
            addRowValues(tableName, columns, values);
        } else if (itemsList instanceof MultiExpressionList) {
            MultiExpressionList mel = (MultiExpressionList) itemsList;
            mel.getExprList().forEach(values -> addRowValues(tableName, columns, values));
        } else {
            throw new UnsupportedOperationException("Type not supported: " + itemsList.getClass());
        }
    }

    private void addRowValues(String tableName, List<Column> columns, ExpressionList values) {
        InsertStatementFormatter table = insertStatements.get(tableName);
        table.addRowValues(columns, values);
    }

    private String formatInsertStatements() {
        StringBuilder sb = new StringBuilder();
        for (InsertStatementFormatter insertStatement : insertStatements.values()) {
            sb.append(insertStatement.format());
            sb.append("\n\n");
        }
        return sb.toString();
    }
}
