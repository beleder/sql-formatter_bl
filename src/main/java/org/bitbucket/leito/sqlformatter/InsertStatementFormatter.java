/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package org.bitbucket.leito.sqlformatter;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.operators.relational.ExpressionList;
import net.sf.jsqlparser.schema.Column;

/**
 * Class that formats a single insert statement with multiple values.
 */
public class InsertStatementFormatter {

    /**
     * the name of the table
     */
    private String tableName;
    /**
     * The columns of this table. The key is the column name.
     */
    private Map<String, ColumnFormatter> columns = new LinkedHashMap<>();
    /**
     * the rows in this insert statement
     */
    private int rowCount = 0;

    public InsertStatementFormatter(String tableName) {
        this.tableName = tableName;
    }

    /**
     * Add a row with values for this insert statement.
     * @param columnsForValues the column list for the values being inserted.
     * @param values the values to insert. Each value must correspond to a column, in the same order.
     */
    public void addRowValues(List<Column> columnsForValues, ExpressionList values) {
        columnsForValues.forEach(c -> this.addColumn(c.getColumnName()));

        List<Expression> expressions = values.getExpressions();
        for (int i = 0; i < columnsForValues.size(); i++) {
            Column columnForValue = columnsForValues.get(i);
            ColumnFormatter columnFormatter = columns.get(columnForValue.getColumnName());
            columnFormatter.addValueForRow(rowCount, expressions.get(i).toString());
        }

        rowCount++;
    }

    /**
     * Formats this insert statement. It adds a header as a comment, and then
     * formats all the columns and values.
     *
     * @return a formatted insert statement.
     */
    public String format() {
        StringBuilder sb = new StringBuilder();
        printHeader(sb);
        printInsertWithColumns(sb);
        printRows(sb);
        return sb.toString();
    }

    private void printHeader(StringBuilder sb) {
        sb.append("-- -----------------------------------------------------------------------------\n");
        sb.append("-- ").append(tableName).append("\n");
        sb.append("-- -----------------------------------------------------------------------------\n");
    }

    private void printInsertWithColumns(StringBuilder sb) {
        sb.append("INSERT INTO ").append(tableName).append("\n(");
        String columnNames = columns.values().stream()
                .map(c -> c.formatHeader())
                .collect(Collectors.joining(", "));
        sb.append(columnNames);
        sb.append(") VALUES\n");
    }

    private void printRows(StringBuilder sb) {
        List<String> rows = new ArrayList<>();
        for (int i = 0; i < rowCount; i++) {
            StringBuilder rowBuilder = new StringBuilder();
            rowBuilder.append("(");
            List<String> rowValues = new ArrayList<>();
            for (ColumnFormatter columnFormatter : columns.values()) {
                rowValues.add(columnFormatter.formatValue(i));
            }
            String formattedRowValues = rowValues.stream().collect(Collectors.joining(", "));
            rowBuilder.append(formattedRowValues);
            rowBuilder.append(")");
            rows.add(rowBuilder.toString());
        }
        String formattedRows = rows.stream().collect(Collectors.joining(",\n"));
        sb.append(formattedRows);
        sb.append(";\n");
    }

    private void addColumn(String columnName) {
        columns.putIfAbsent(columnName, new ColumnFormatter(columnName));
    }

}
