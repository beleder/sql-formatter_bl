/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package org.bitbucket.leito.sqlformatter;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Formats a single column of an insert statement.
 */
public class ColumnFormatter {
    /** Value used when the row has now value in this column. */
    private static final String UNKNOWN_VALUE = "NULL";

    /** The name of the column. */
    private String name;
    /** The values for the column. The key is the row number. */
    private Map<Integer, String> values = new LinkedHashMap<>();
    /** The largest length that a value of this column has. */
    private int maxColumnLength;

    public ColumnFormatter(String name) {
        this.name = name;
        maxColumnLength = name.length() > UNKNOWN_VALUE.length() ? name.length() : UNKNOWN_VALUE.length();
    }

    /** Formats the header of this column for the insert statement. */
    public String formatHeader() {
        return padRight(name);
    }

    /** Formats the value of this column for the given row. */
    public String formatValue(int row) {
        StringBuilder sb = new StringBuilder();
        String value = values.get(row);
        if (value == null) {
            value = UNKNOWN_VALUE;
        }
        sb.append(padRight(value));
        return sb.toString();
    }

    /** Adds a value to this column for the given row. */
    public void addValueForRow(int row, String value) {
        values.putIfAbsent(row, value);
        maxColumnLength = value.length() > maxColumnLength ? value.length() : maxColumnLength;
    }

    /** Pads a string to the right with empty spaces. */
    private String padRight(String s) {
        return String.format("%1$-" + maxColumnLength + "s", s);
    }

}
