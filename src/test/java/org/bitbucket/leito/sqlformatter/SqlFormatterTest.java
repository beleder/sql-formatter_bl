/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package org.bitbucket.leito.sqlformatter;

import org.apache.commons.io.IOUtils;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.Before;

public class SqlFormatterTest {

    private SqlFormatter formatter;

    private String readFromResource(String name) throws Exception {
        return IOUtils.toString(App.class.getResourceAsStream(name), "utf-8");
    }

    @Before
    public void setup() {
        formatter = new SqlFormatter();
    }

    private void assertTableComment(String tableName, String sql) {
        assertTrue(sql.contains("\n-- " + tableName + "\n"));
    }

    @Test
    public void format_complexFile_formatsSql() throws Exception {
        String format = formatter.format(readFromResource("/test.sql"));
        System.out.println(format);
        assertTrue(format.length() > 100);

        assertTableComment("movie", format);
        assertTrue(format.contains("INSERT INTO movie\n(id  , title                       , release_date, enabled, id_genre) VALUES\n"));
        assertTrue(format.contains("(2   , 'The Place Beyond The Pines', '2011-11-04', false  , NULL    ),\n"));
        assertTrue(format.contains("(3   , 'Grand Hotel Budapest'      , '2011-11-04', false  , 5       ),\n"));
        assertTrue(format.contains("(6   , 'Inglorious Basterds'       , NULL        , true   , NULL    ),\n"));
        assertTrue(format.contains("(7   , 'áéíóú // ÁÉÍÓÚ // ñÑ'      , '2016-11-04', false  , NULL    ),\n"));
        assertTrue(format.contains("(999 , 'The Last Movie'            , NULL        , NULL   , NULL    );\n"));

        assertTableComment("genre", format);
        assertTrue(format.contains("INSERT INTO genre\n(id  , description , enabled) VALUES\n"));
        assertTrue(format.contains("(2   , 'Terror'    , NULL   ),\n"));
        assertTrue(format.contains("(7   , 'Documental', false  ),\n"));
        assertTrue(format.contains("(99  , 'Last Genre', NULL   );\n"));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void format_withCreateStatement_throwsException() throws Exception {
        String format = formatter.format(readFromResource("/create.sql"));
        System.out.println(format);
        assertTrue(format.length() > 100);
    }

}
