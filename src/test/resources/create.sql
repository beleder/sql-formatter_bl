-- -----------------------------------------------------------------------------
-- Test file for SqlFormatter
-- CREATE statement not supported.
-- -----------------------------------------------------------------------------

create table movie (
    id bigint primary key,
    title varchar(255) not null,
    release_date datetime,
    enabled boolean
);

insert into movie (id, title, release_date, enabled)
values (1, 'Cloud Atlas', '2009-12-11', true);
